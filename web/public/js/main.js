// structure: { id, title, description, has structures }
// interface: { id, title, description, interfaces }
// procedure: { id, interface, in_structures, out_structures }

// node: { procedure_id, in_structure_connections }


// Structure builder
// Procedure builder
// Node builder

// On index generate unique url
// Show join button with url box
var state = {
	strucutres: [],
	interfaces: [],
	procedures: [],
};
var player = {
	x: 0,
	y: 0,
	idx: 0,
};

function idx() { return --player.idx; }

function create_node_view() {
	let node = {};

}



var _body = document.getElementById("_body");
var _canvas = document.getElementById("_canvas");
var _inspector = document.getElementById("_inspector");
var _inspector_index = document.getElementById("_inspector_index").innerHTML;
var _inspector_interface = document.getElementById("_inspector_interface").innerHTML;
var _inspector_procedure = document.getElementById("_inspector_procedure").innerHTML;
var _inspector_structure = document.getElementById("_inspector_structure").innerHTML;
function find(element, key) {
	let walker = document.createTreeWalker(element, NodeFilter.SHOW_ELEMENT);
	let current = walker.currentNode;
	while (current) {
		if (current.getAttribute("key") === key)
			return current;
		current = walker.nextNode();
	}
	return null;
}


function create_interface() {
	let interface = {};
	interface.id          = idx();
	interface.name        = find(_inspector, "title").value;
	interface.description = find(_inspector, "description").value;
	state.interfaces.push(interface);
}
function create_procedure() {
	let procedure = {};
	procedure.id = idx();
	procedure.name = find(_inspector, "title").value;
	procedure.description = find(_inspector, "description").value;

	let 
}


function nav_index() {
	_inspector.innerHTML = _inspector_index;
}
function nav_structure(structure) {
	_inspector.innerHTML = _inspector_structure;
	if (structure) {
		let title       = find(_inspector, "title");
		let description = find(_inspector, "description");
		title.value       = structure.title;
		description.value = structure.description;
	}
}
function nav_procedure(procedure) {
	_inspector.innerHTML = _inspector_procedure;
	if (procedure) {
		let title       = find(_inspector, "title");
		let description = find(_inspector, "description");
		title.value       = procedure.title;
		description.value = procedure.description;
	}
}
function nav_interface(interface) {
	_inspector.innerHTML = _inspector_interface;
	if (interface) {
		let title       = find(_inspector, "title");
		let description = find(_inspector, "description");
		title.value       = interface.title;
		description.value = interface.description;
	}
}
nav_index();

document.addEventListener("contextmenu", function(e) {
	let target  = e.target;
	let context = target.getAttribute("context");
	if (context) {
		switch(context) {
			case "create": {
				
			} break;
		}
		e.preventDefault();
	}
});





var socket; {
	let protocol = location.protocol === "https:" ? "wss://" : "ws://";
	let host = location.host;
	let path = location.pathname;
	if (path[path.length - 1] === '/') path += "connect";
	else path += "/connect"
	socket = new WebSocket(protocol+host+path);
	socket.onopen = function() {
		console.log("open");
	};
	socket.onclose = function() {
		console.log("close");
	};
	socket.onmessage = function(message) {
		console.log(message);
	};
	socket.onerror = function(error) {
		console.error(error);
	};
}







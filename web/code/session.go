package code
import (
	"net/http"
	"strings"
	"time"
)

type Session struct {
	ID UUID
	IP string
	Expire time.Time
	Values map[string]interface{}
}

func CreateSession(sessions map[UUID]*Session, lifetime time.Duration, w http.ResponseWriter, r *http.Request) UUID {
	ip := r.RemoteAddr
	fwdAddress := r.Header.Get("X-Forwarded-For")
	if fwdAddress != "" {
		ip = fwdAddress
		ips := strings.Split(fwdAddress, ", ")
		if len(ips) > 1 {
			ip = ips[0]
		}
	}

	ip = ip[:strings.LastIndex(ip, ":")]
	uuid, err := GenerateUUID(nil)
	if err != nil {
		panic(err)
	}
	
	expires := time.Now().Add(lifetime)
	cookie := &http.Cookie{
		Name: "sid",
		Value: StringFromUUID(uuid),
		Expires: expires,
		Path: "/",
		HttpOnly: true,
	}
	http.SetCookie(w, cookie)
	sessions[uuid] = &Session{
		ID: uuid,
		IP: ip,
		Expire: expires,
		Values:  make(map[string]interface{}),
	}
	return uuid
}

// Get returns the session belonging to the given request
// ; else creates a new session for the request and returns this session.
func SessionOf(sessions map[UUID]*Session, lifetime time.Duration, w http.ResponseWriter, r *http.Request) *Session {
	var uuid UUID
	cookie, err := r.Cookie("sid")
	if err == nil {
		uuid, _ = UUIDFromString(cookie.Value)
	}
	if uuid == EmptyUUID {
		uuid = CreateSession(sessions, lifetime, w, r)
	}
	
	if session, exists := sessions[uuid]; exists {
		ip := r.RemoteAddr
		fwdAddress := r.Header.Get("X-Forwarded-For")
		if fwdAddress != "" {
			ip = fwdAddress
			ips := strings.Split(fwdAddress, ", ")
			if len(ips) > 1 {
				ip = ips[0]
			}
		}
		
		ip = ip[:strings.LastIndex(ip, ":")]
		if session.IP == ip {
			return session
		}
	}
	uuid = CreateSession(sessions, lifetime, w, r)
	return sessions[uuid]
}


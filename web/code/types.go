package code
import (
	"sync"
	"time"
	"github.com/gorilla/websocket"
)

const (
	ROOM_LIFETIME time.Duration = time.Hour * 32
)


type Client struct {
	Name string
	Socket *websocket.Conn
}

type Room struct {
	Identifier UUID
	Clients []Client
	Expires time.Time
	Mutex sync.Mutex
	Idx int
}

type Server struct {
	Upgrader websocket.Upgrader
	Rooms []Room
}


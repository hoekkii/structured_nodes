package code
import (
	"os"
	"log"
	"fmt"
	"time"
	"strings"
	"regexp"
	"net/http"
	"github.com/gorilla/websocket"
)

var RoutePublic *regexp.Regexp = regexp.MustCompile(`^/\w+(/\w+)?/[_\-a-zA-Z0-9]+\.(min\.js|js|mp3|ogg|jpg|jpeg|png|svg|woff|ttf|css|min\.css)$`)
var RouteDefault *regexp.Regexp = regexp.MustCompile(`^(/?([_\-a-zA-Z0-9]+))*$`)

func ServerError(w http.ResponseWriter, r *http.Request, code int, err string, a ...interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	fmt.Fprintf(w, err, a...)
	log.Printf("ERROR at %v: %v\n", r.URL.Path, err)
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(404)
	fmt.Fprint(w, `{"message":"Path not found."}`)
}

func (server *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	if path == "" || path == "/" {
		CreateRoom(server, w, r)
		return
	}

	if path == "/favicon.ico" {
		http.ServeFile(w, r, "./public/favicon.ico")
		return
	}

	if path == "/robots.txt" {
		http.ServeFile(w, r, "./public/robots.txt")
		return
	}

	if (RouteDefault.MatchString(path)) {
		items := strings.Split(path, "/")[1:]
		count := len(items)
		if items[0] == "new" {
			CreateRoom(server, w, r)
			return
		} else if count >= 1 {
			room := RoomById(server, items[0])
			if room != nil {
				if count == 1 {
					JoinRoom(server, w, r, room)
					return
				} else if items[1] == "connect" {
					Connect(server, w, r, room)
					return
				}
			}
		}
	} else if RoutePublic.MatchString(path) {
		file := "./public" + path
		if _, err := os.Stat(file); err == nil {
			http.ServeFile(w, r, file)
			return
		}
	}

	NotFound(w, r)
}



func Start() {
	port := ":80"
	args := os.Args
	argt := 0
	for _, arg := range args {
		if argt == 0 {
			if arg == "-h" || arg == "--help" {
				log.Printf("-h --help     Print help")
				log.Printf("-p --port     Set the server port")
				log.Printf("")
				log.Printf("")
				log.Printf("")
				return
			} else if arg == "-p" || arg == "--port" {
				argt = 1
			}
		} else if argt == 1 {
			if arg[0] == ':' {
				port = arg
			} else {
				port = ":"+arg
			}
			argt = 0
		}
	}

	// Initialize server
	server := &Server {
		Rooms: make([]Room, 0, 128),
		Upgrader: websocket.Upgrader{},
	}
	http_server := &http.Server {
		Addr: port,
		Handler: server,
		ReadTimeout: 10 * time.Second,
		WriteTimeout: 10 * time.Second,
		MaxHeaderBytes: 1048576,
	}

	// Start server
	log.Printf("Server running at  %v\n", http_server.Addr)
	err := http_server.ListenAndServe()
	if err != nil {
		log.Fatal("An error has occurred: %v", err)
	}
}


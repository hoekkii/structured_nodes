package code
import (
	"log"
	"time"
	"net/http"
	//"github.com/gorilla/websocket"
)

func RoomById(server *Server, id string) *Room {
	uuid, err := UUIDFromString(id)
	if err != nil {
		return nil
	}

	rooms := server.Rooms
	for i := range rooms {
		room := &rooms[i]
		if room.Identifier == uuid {
			return room
		}
	}
	return nil
}
func CreateRoom(server *Server, w http.ResponseWriter, r *http.Request) {
	var identifier UUID
	generate_uuid: for {
		uuid, err := GenerateUUID(nil)
		if err == nil {
			rooms := server.Rooms
			for _, room := range rooms {
				if room.Identifier == uuid {
					continue generate_uuid
				}
			}
			identifier = uuid
			break
		} else {
			ServerError(w, r, 1001, "Error generating UUID: %v", err)
			return
		}
	}
	result := Room {
		Identifier: identifier,
		Clients: make([]Client, 0, 32),
		Expires: time.Now().Add(ROOM_LIFETIME),
	}
	server.Rooms = append(server.Rooms, result)
	url := "/"+StringFromUUID(identifier)
	http.Redirect(w, r, url, http.StatusFound)
}
func JoinRoom(server *Server, w http.ResponseWriter, r *http.Request, room *Room) {
	http.ServeFile(w, r, "public/index.html")
}
func Close(server *Server, room *Room) {
	clients := room.Clients
	for _, client := range clients {
		client.Socket.Close()
	}
	room.Clients = clients[:0]

	identifier := room.Identifier
	rooms := server.Rooms
	for i, other := range rooms {
		if other.Identifier == identifier {
			last := len(rooms) - 1
			rooms[i] = rooms[last]
			server.Rooms = rooms[:last]
		}
	}
}
func Connect(server *Server, w http.ResponseWriter, r *http.Request, room *Room) {
	connection, err := server.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("ERROR: %v\n", err)
		ServerError(w, r, 500, `{"message":"Failed to create a connection.","code":1301}`)
		return
	}
	if len(room.Clients) == cap(room.Clients) {
		ServerError(w, r, 500, `{"message":"The room you are trying to connect to is full.","code":1302}`)
		return
	}
	type message_t struct {
		Action string `json:"action"`
	}

	go func() {
		loop: for {
			var message message_t
			if err := connection.ReadJSON(&message); err != nil {
				log.Printf("[E1303] %v", err)
				break loop
			}
			log.Printf("[I1304] %v", message)
			now := time.Now()
			room.Mutex.Lock()
			room.Expires = now.Add(ROOM_LIFETIME)
			room.Mutex.Unlock()
		}
	}()
	connection.Close()
}


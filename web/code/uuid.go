package code
import (
	"io"
	"fmt"
	"encoding/hex"
	"crypto/rand"
)

type UUID [16]byte

var EmptyUUID UUID
var UUIDRandomizer = rand.Reader
var UUIDHexValues = [256] byte { // 0.. 9 - a.. f - A.. F
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	  0,   1,   2,   3,   4,   5,   6,   7,   8,   9, 255, 255, 255, 255, 255, 255,
	255,  10,  11,  12,  13,  14,  15, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255,  10,  11,  12,  13,  14,  15, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
}

func GenerateUUID(r io.Reader) (UUID, error) {
	var uuid UUID
	if (r == nil) {
		r = UUIDRandomizer
	}
	_, err := io.ReadFull(r, uuid[:])
	if err != nil {
		return EmptyUUID, err
	}
	uuid[6] = (uuid[6] & 0x0f) | 0x40 // Version 4
	uuid[8] = (uuid[8] & 0x3f) | 0x80 // Variant is 10
	return uuid, nil
}

func EncodeHexFromUUID(destination []byte, uuid UUID) {
	hex.Encode(destination, uuid[:4])
	destination[8] = '-'
	hex.Encode(destination[9:13], uuid[4:6])
	destination[13] = '-'
	hex.Encode(destination[14:18], uuid[6:8])
	destination[18] = '-'
	hex.Encode(destination[19:23], uuid[8:10])
	destination[23] = '-'
	hex.Encode(destination[24:], uuid[10:])
}

func StringFromUUID(uuid UUID) string {
	var buf [36]byte
	EncodeHexFromUUID(buf[:], uuid)
	return string(buf[:])
}

func UUIDFromString(s string) (UUID, error) {
	if (len(s) != 36 || s[8] != '-' || s[13] != '-' || s[18] != '-' || s[23] != '-') {
		return EmptyUUID, fmt.Errorf("uuid string has not the format of xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx: %s", s)
	}
	
	var uuid UUID
	for i, x := range [16] int { 0, 2, 4, 6, /*-*/ 9, 11, /*-*/ 14, 16, /*-*/ 19, 21, /*-*/ 24, 26, 28, 30, 32, 34 } {
		a := UUIDHexValues[s[x]]
		b := UUIDHexValues[s[x+1]]
		if a > 16 || b > 16 {
			return EmptyUUID, fmt.Errorf("uuid character %c%c is not valid, it should be 0..9-a..f-A..F: %s", a, b, s)
		}
		uuid[i] = (a << 4) | b
	}
	
	return uuid, nil
}

